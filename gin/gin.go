package gin

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/ariatel_ats/tools/dynamicPermissions"

	"github.com/gin-gonic/gin"
	"github.com/luraproject/lura/config"
	"github.com/luraproject/lura/logging"
	"gopkg.in/square/go-jose.v2/jwt"
)

type Permission struct {
	Name string `json:"name"`
}

type PostBody struct {
	Name        string       `json:"name"`
	Permissions []Permission `json:"permissions"`
}

func Register(cfg *config.ServiceConfig, logger logging.Logger, engine *gin.Engine) {
	permConfig, err := dynamicPermissions.GetPermissionsConfig(cfg.ExtraConfig, logger)
	if err != nil {
		return
	}
	logger.Info("DynamicPermissions Enabled for the endpoint")
	engine.Use(PermissionValidator(permConfig, logger))
}

func PermissionValidator(permConfig *dynamicPermissions.PermissionConfig, logger logging.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := c.Request.Header.Get("Authorization")
		if auth == "" {
			logger.Error("DynamicPermissions: Authorization Header Field empty.")
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		token := strings.TrimPrefix(auth, "Bearer ")
		if token == auth {
			logger.Error("DynamicPermissions: TrimPrefix process does not work well.")
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		role, err := GetRoleFromPayload(token)
		if err != nil {
			logger.Error("DynamicPermissions: Can't get the role from the token payload.")
			logger.Error(err.Error())
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		permission := Permission{permConfig.PermissionCode}
		permissions := []Permission{permission}
		postBody := PostBody{role, permissions}

		body, err := json.Marshal(postBody)
		if err != nil {
			logger.Error(err.Error())
			c.AbortWithStatus(http.StatusForbidden)
			return
		}
		request, err := http.NewRequest("POST", permConfig.URI, bytes.NewBuffer(body))
		if err != nil {
			logger.Error(err.Error())
			c.AbortWithStatus(http.StatusForbidden)
			return
		}
		request.Header.Set("Content-Type", "application/json; charset=UTF-8")

		client := &http.Client{}
		response, error := client.Do(request)
		if error != nil {
			logger.Error(err.Error())
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		responseStatus, err := strconv.Atoi(response.Status)
		if error != nil {
			logger.Error(err.Error())
			c.AbortWithStatus(http.StatusForbidden)
			return
		}

		if responseStatus == http.StatusOK {
			c.Next()
		}
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
}

func GetRoleFromPayload(token string) (string, error) {
	var claims map[string]interface{}

	payload, err := jwt.ParseSigned(token)
	if err != nil {
		return "", err
	}

	err = payload.UnsafeClaimsWithoutVerification(&claims)
	if err != nil {
		return "", err
	}

	roleSlice := claims["role"].([]interface{})
	role := roleSlice[0].(string)
	return role, nil
}
