package dynamicPermissions

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/luraproject/lura/config"
	"github.com/luraproject/lura/logging"
)

const (
	NameSpace = "gitlab.com/ariatel_ats/tools/dynamicPermissions"
)

var (
	ErrNoValidatorCfg    = errors.New("DynamicPermission: no validator config.")
	ErrInsecureJWKSource = errors.New("DynamicPermission: JWK client is using an insecure connection to the Permission service.")
)

type PermissionConfig struct {
	PermissionCode string `json:"permission_code"`
	URI            string `json:"permission_url"`
}

func GetPermissionsConfig(e config.ExtraConfig, logger logging.Logger) (*PermissionConfig, error) {
	tmp, ok := e[NameSpace].(map[string]string)
	if !ok {
		return nil, ErrNoValidatorCfg
	}

	data, err := json.Marshal(tmp)
	if err != nil {
		logger.Error(fmt.Sprintf("Marshal krakend-dynamicPermissions config error: %s", err.Error()))
		return nil, ErrNoValidatorCfg
	}
	var permCfg PermissionConfig
	if err := json.Unmarshal(data, &permCfg); err != nil {
		logger.Error(fmt.Sprintf("Unmarshal krakend-dynamicPermissions config error: %s", err.Error()))
		return nil, err
	}

	if !strings.HasPrefix(permCfg.URI, "https://") {
		return &permCfg, ErrInsecureJWKSource
	}
	return &permCfg, nil
}
